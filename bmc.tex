
\section{有界モデル検査}
\label{s:bmc}
有界モデル検査 (Bounded Model Checking, BMC) は, システムの遷移を一定の回数$k$に限定したうえで, 検査を行う手法である\cite{Biere1999}.
具体的には，システム, 性質 (安全性), 探索範囲 $k$を入力とし，安全性の必要条件や十分条件を述語論理式にエンコードし，
その述語論理式の成否をSATソルバーやSMTソルバーによって判定し，検査をおこなう手法である．
以下，本節ではその概要を説明する．

\subsection{有限状態遷移系と安全性}
\label{s:finite-state}
有限状態遷移系 (システム) は, 初期状態を定める条件$I$と, ある状態から別の状態への遷移関係$T$からなる.
本研究では, システムの各状態を整数のベクトルで表すことができるものとする.

状態に関する性質を$P$とすると，
あるシステム$(I,T)$が性質$P$について安全であるとは, 
$I$が定める初期状態から遷移関係$T$によって到達可能なすべての状態が, 性質$P$を満たすことを言う.
逆に, 到達可能な状態が$\neg P$を満たすシステムは, 不具合 (バグ) を含んだシステムと言える.


\begin{figure}[t]
\includegraphics[width=0.48 \textwidth]{figure/framework.eps}
\caption{提案する実装の概要}
\label{f:framework} 
\end{figure}

\subsection{有界モデル検査}
\label{s:bmc:bmc}
%有界モデル検査 (Bounded Model Checking, BMC) は, システムの遷移を一定の回数$k$に限定したうえで, 検査を行う手法である\cite{Biere1999}.
%具体的には，システム$(I,T)$, 性質$P$, 自然数$k$を入力とし，安全性の必要条件や十分条件を述語論理式にエンコードし，
%その述語論理式の成否をSATソルバーやSMTソルバーによって判定し，検査をおこなう手法である．
%以下，本節ではその概要を説明する．

ここでは，システム ($I$, $T$)，性質$P$，探索範囲 $k$ から，
システムが$k$ステップ目まで (性質$P$について) 安全であることを検査する方法を示す．

システムをエンコードするにあたり，実行中の状態を表すために整数ベクトルの変数$s_0,\ldots, s_k$を用意する．
また状態の列$s_i,\ldots,s_j$を$s_{[i..j]}$で表すことにする.

まず，$s$, $s'$を状態変数としたとき，
述語論理式$I(s)$, $T(s, s')$, $P(s)$によりそれぞれ，$s$が初期状態であること, 
$s$と$s'$が遷移関係にあること，$s$が$P$を満たすことをエンコードできるものとする.
%
つぎに, $s_{[0..k]}$が状態$s_0$から$k$ステップ目の状態$s_k$までの実行パスであることを
式$\mathit{path}(s_{[0..k]})$としてエンコードできる:
\begin{align} %path
 \label{eq:path}
    \mathit{path}(s_{[0..k]})  ~:=~  \bigwedge_{0 \leq i < k}T(s_i,s_{i+1})
\end{align} 
さらに, システムの$k$ステップ以内の状態が$P$を満たさないことを
以下のようにエンコードできる:
\begin{align} %naive
 \label{eq:naive}
  I(s_0) \ \land \ path(s_{[0..k]}) \ \land  \bigvee_{0 \leq i \leq k} \neg P(s_i) 
\end{align} 
%
SAT/SMTソルバーにより条件
\begin{align} %Biere
    \label{eq:naive:post}
    \neg\exists s_{[0..k]}. ~ \text{(式\eqref{eq:naive})}
\end{align} 
が成り立つことを示せれば,
システムが$k$ステップまでは安全であることが分かる.
この式\eqref{eq:naive:post}による検査では, $k$ステップより後の状態を含むすべての状態については安全であることは言えない. 

\subsection{Sheeranらの方法1}
\label{s:bmc:Induction}
システムのすべての状態について安全性を示したり, 
検査可能なシステムを増やしたり, エンコード式のサイズを小さくしたりするために, 
さまざまなBMCアルゴリズム (BMC法) が提案されている\cite{Sheeran2000, McMillan2003, Bradley2011}.
Sheeranら\cite{Sheeran2000}は, 帰納法に基づき簡潔な式にエンコードする6つの方法を提案している.
ここでは, 方法1による安全性の検査について述べる.

まず, システムが$k$ステップ以内で安全であることを
\begin{align} %loopFree
    \label{eq:k-safety}
    \bigwedge_{0 \leq i \leq k} \bigl(\neg\exists s_{[0..i]}. ~ I(s_0) ~\land~ \mathit{path}(s_{[0..i]}) ~\land~ \neg P(s_i) \bigr) 
\end{align} 
とエンコードすることができ, SAT/SMTソルバーで各$i$について部分式が充足不能であることを確認できれば, 示すことができる.
%
つぎに, 実行パス$s_{[0..k]}$に状態の重複がない (閉路がない) ことを式$\mathit{loopFree}(s_{[0..k]})$としてエンコードする:
\begin{align} %loopFree
 \label{eq:loopfree}
    \mathit{loopFree}(s_{[0..k]})  ~:=~  \mathit{path}(s_{[0..k]}) ~\land \bigwedge_{0 \leq i <  j \leq k} s_i \neq s_j 
\end{align} 
%
本稿では具体的なアルゴリズムは省略するが，
方法1は最終的に以下の条件が成り立つことを確認する (方法1の事後条件):
\begin{multline}
\label{eq:Sheeran1}
    \Bigl(~(\neg \exists s_{[0..k]}. ~ I(s_0) ~\land~ \mathit{loopFree}(s_{[0..k]})) ~\lor~ \\ \qquad
    (\neg\exists s_{[0..k]}. ~ \mathit{loopFree}(s_{[0..k]})  ~\land~ \neg P(s_k)) ~\Bigr) \\
    \land \ (式\eqref{eq:k-safety})
\end{multline}
上記が成り立つとき, すべての状態についてシステムが安全であることが言える.
%
実際のアルゴリズムでは, $k$を徐々に増やしながら式\eqref{eq:Sheeran1}の各部分式を評価し, 
効率よく検査を実施する.


\subsection{$k$-Induction法}

$k$-Induction の考え方に基づき，方法1 (式\eqref{eq:Sheeran1}) を拡張することができる．
%
本節でも具体的なアルゴリズムは省略するが，
以下の条件が成り立つことを確認する ($k$-Induction法の事後条件):
\begin{multline}%k-Induction
\label{eq:k-Induction}
   \Bigl(~(\neg \exists s_{[0..k]}. ~ I(s_0) ~\land~ \mathit{loopFree}(s_{[0..k]})) ~\lor~   \\ 
    (\neg\exists s_{[0..k]}. \ \mathit{loopFree}(s_{[0..k]}) ~\land \bigwedge_{0 \leq i \leq k-1}P(s_i) ~\land~ \neg P(s_k))~\Bigr)  \\
    \land \ (式\eqref{eq:k-safety})
\end{multline}
上記が成り立つとき，すべての状態についてシステムが安全であることが言える．
%
この式の各部分式は以下の式と論理的に等価である:
%
\vspace*{2em}
\begin{itemize}%k-Induction
\item[~~(a)~] $\forall s_{[0..k]}. ~ I(s_0) \to \neg \mathit{loopFree}(s_{[0..k]})$ 
\item[~~(b)~] $\forall s_{[0..k]}. ~ \mathit{loopFree}(s_{[0..k]}) ~\land~ \bigwedge_{0 \leq i \leq k-1}P(s_i) \to P(s_k)$
\item[~~(c)~] $\forall s_{[0..k]}. ~ \bigwedge_{0 \leq i \leq k}(I (s_0) ~\land~ \mathit{path}(s_{[0..i]}) \to P(s_i))$
\end{itemize}
各部分式はそれぞれ，
%
\begin{itemize}
\item[~~(a)~] 任意の状態$s_{[0..k]}$について，$s_0$が初期状態の制約$I$を満たすならば，$s_0$から$s_k$の実行パスは閉路となる．
\item[~~(b)~] 任意の状態$s_{[0..k]}$について，閉路を持たない$s_0$から$s_k$の実行パスの$s_0$から$s_{k-1}$までが安全性$P$を満たしているならば，
	             $s_k$は$P$を満たしている．
\item[~~(c)~]  システムは，$k$ステップ以内において安全である．
\end{itemize}
を表している．
















